import { createAction, createActionGroup, props } from '@ngrx/store';
import { CartItem } from 'src/app/app-interfaces';

export const bookAddedSuccess = createAction(
    '[Cart/API] Book added to cart successfully'
);

export const CartPageActions = createActionGroup({
    source: 'Cart/Page',
    events: {
        'Decrease number of item in cart': props<{ cartItem: CartItem }>(),
        'Increase number of item in cart': props<{ cartItem: CartItem }>(),
        'Remove item from cart': props<{ cartItem: CartItem }>()
    }
})
