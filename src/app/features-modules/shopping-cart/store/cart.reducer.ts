import { createReducer, on } from '@ngrx/store';

import { Book, CartFeatureState, CartItem } from '../../../app-interfaces';
import { CatalogPageActions } from '../../catalog/store/catalog-page.actions';
import { CartPageActions } from './cart-page.actions';

export const initialState: CartFeatureState = {
    cartItems: [],
    numberOfItems: 0
};

const getNumberOfItems = (cartItems: CartItem[]): number => {
    return cartItems.reduce((partialSum, cartItem) => partialSum + cartItem.qty, 0);
}

export const cartReducer = createReducer(
    initialState,
    on(CatalogPageActions.addItemToCart, (store: CartFeatureState, result) => {
        const existingItem = store.cartItems.find(({id}: Partial<Book>) => id === result.item.id);

        return {
            ...store,
            cartItems: store.cartItems.map((cartItem: any) => cartItem.item.id !== result.item.id
                ? cartItem
                : { ...cartItem, qty: cartItem.qty + 1 }
            ).concat(existingItem ? [] : [{ id: result.item.id, item: result.item, qty: 1 }]), numberOfItems: store.numberOfItems + 1
        }
    }),
    on(CartPageActions.decreaseNumberOfItemInCart, (store: CartFeatureState, result) => {
        return {
            ...store,
            cartItems: store.cartItems.map((cartItem: any) => cartItem.id !== result.cartItem.id
                ? cartItem
                : { ...cartItem, qty: cartItem.qty - 1 }
            ).filter(({qty}) => qty > 0), numberOfItems: store.numberOfItems - 1
        }
    }),
    on(CartPageActions.increaseNumberOfItemInCart, (store: CartFeatureState, result) => {
        return {
            ...store,
            cartItems: store.cartItems.map((cartItem: any) => cartItem.id !== result.cartItem.id
                ? cartItem
                : { ...cartItem, qty: cartItem.qty + 1 }
            ).filter(({qty}) => qty > 0), numberOfItems: store.numberOfItems + 1
        }
    }),
    on(CartPageActions.removeItemFromCart, (store: CartFeatureState, result) => {
        const cartItems = [...store.cartItems.filter((item: any) => item.id !== result.cartItem.id)];
        return {
            cartItems,
            numberOfItems: getNumberOfItems(cartItems)
        }
    }),
);
