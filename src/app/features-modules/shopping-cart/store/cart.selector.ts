import { createSelector } from '@ngrx/store';
import { CartFeatureState, CartItem } from '../../../app-interfaces';

export const selectCartState = (state: CartFeatureState) => state;

export const selectCartItems = createSelector(
    selectCartState,
    (state: any | undefined) => {
        return state?.cartFeature?.cartItems;
    }
);

export const selectCartTotalPrice = createSelector(
    selectCartState,
    (state: any | undefined) =>
        state.cartFeature?.cartItems.reduce(
            (accumulator: number, cartItem: CartItem) => {
                return cartItem.item.saleInfo.listPrice?.amount ?
                    accumulator + (cartItem.qty * cartItem.item.saleInfo.listPrice?.amount)
                    :
                    0;
            }, 0
        )
);
