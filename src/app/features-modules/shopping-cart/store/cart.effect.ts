import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { CatalogPageActions } from "../../catalog/store/catalog-page.actions";
import { map } from "rxjs";
import { bookAddedSuccess } from "./cart-page.actions";

@Injectable()
export class CartEffect {

    addItemToCart$ = createEffect(() => this.actions$.pipe(
        ofType(CatalogPageActions.addItemToCart),
        // Call Server via service here
        map(_ => (bookAddedSuccess()))
    ));

    constructor(private actions$: Actions) {}
}
