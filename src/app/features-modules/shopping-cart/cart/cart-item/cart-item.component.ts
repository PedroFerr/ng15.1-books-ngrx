import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CartItem } from 'src/app/app-interfaces';

@Component({
    selector: 'app-cart-item',
    templateUrl: './cart-item.component.html',
    styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent {

    @Input() cartItem: any | undefined;
    @Output() decreaseItemInCartEvent = new EventEmitter<any>();
    @Output() increaseItemInCartEvent = new EventEmitter<CartItem>();
    @Output() removeItemInCartEvent = new EventEmitter<CartItem>();

    decreaseItemInCart() {
        this.decreaseItemInCartEvent.emit(this.cartItem);
    }

    increaseItemInCart() {
        this.increaseItemInCartEvent.emit(this.cartItem);
    }

    removeItemFromCart() {
        this.removeItemInCartEvent.emit(this.cartItem);
    }

}
