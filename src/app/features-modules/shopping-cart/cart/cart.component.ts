import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Store } from '@ngrx/store';

import { Observable, of } from 'rxjs';

import { CartFeatureState, CartItem } from '../../../app-interfaces';

import { selectCartItems, selectCartTotalPrice } from '../store/cart.selector';
import { CartPageActions } from '../store/cart-page.actions';
import { selectNumberOfCartItems } from 'src/app/shell/core.selector';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent {
    cartItems$: Observable<CartItem[]> /* = of([]); */
    totalPrice$: Observable<number> /* = of(0); */
    numberOfCartItems$: Observable<number>;


    @Input() cartItem?: CartItem /* = {id:'', item: {} as Book, qty: 0}; */
    @Output() closeEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(private store: Store<CartFeatureState>) {
        this.cartItems$ = this.store.select(selectCartItems);
        this.totalPrice$ = this.store.select(selectCartTotalPrice);
        this.numberOfCartItems$ = this.store.select(selectNumberOfCartItems);
    }

    decreaseItem(cartItem: CartItem) {
        this.store.dispatch(CartPageActions.decreaseNumberOfItemInCart({ cartItem }))
    }

    increaseItem(cartItem: CartItem) {
        this.store.dispatch(CartPageActions.increaseNumberOfItemInCart({ cartItem }));
    }

    removeItem(cartItem: CartItem) {
        this.store.dispatch(CartPageActions.removeItemFromCart({ cartItem }));
    }

    closeCart() {
        this.closeEvent.emit(true);
    }
}
