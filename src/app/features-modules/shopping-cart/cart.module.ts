import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { CartEffect } from './store/cart.effect';
import { cartReducer } from './store/cart.reducer';

import { AppMaterialModule } from 'src/app/app-material.module';

import { CartComponent } from './cart/cart.component';
import { CartItemComponent } from './cart/cart-item/cart-item.component';

import { NgPipesModule } from 'ngx-pipes';


@NgModule({
    declarations: [
        CartComponent,
        CartItemComponent
    ],
    exports: [
        CartComponent
    ],
    imports: [
        CommonModule,
        AppMaterialModule,
        StoreModule.forFeature('cartFeature', cartReducer),
        EffectsModule.forFeature([CartEffect]),
        NgPipesModule,
    ]
})
export class CartModule {
}
