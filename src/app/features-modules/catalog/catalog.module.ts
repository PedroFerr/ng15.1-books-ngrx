import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { CatalogEffect } from './store/catalog.effect';
import { catalogReducer } from './store/catalog.reducer';

import { CatalogPageComponent } from './catalog-page/catalog-page.component';
import { AppMaterialModule } from 'src/app/app-material.module';

@NgModule({
    declarations: [
        CatalogPageComponent,
    ],
    exports: [
        CatalogPageComponent
    ],
    imports: [
        CommonModule,
        AppMaterialModule,
        StoreModule.forFeature('booksFeature', catalogReducer),
        EffectsModule.forFeature([CatalogEffect]),
    ]
})
export class CatalogModule { }
