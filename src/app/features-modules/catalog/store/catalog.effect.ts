import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import { GoogleBooksService } from 'src/app/services/books-service';
import { CatalogPageActions, booksLoadedSuccess } from './catalog-page.actions';

@Injectable()
export class CatalogEffect {

    loadArticles$ = createEffect(() => this.actions$.pipe(
        ofType(CatalogPageActions.getBooks),
        mergeMap(() => this.bookService.getBooks()
            .pipe(
                map(items => (booksLoadedSuccess({ data: items }))),
                catchError(() => EMPTY)
            )
        )
    ));

    constructor(
        private actions$: Actions,
        private bookService: GoogleBooksService
    ) { }
}
