import { createSelector } from '@ngrx/store';
import { BooksFeatureState } from 'src/app/app-interfaces';


export const selectBooksState = (state: any) => state.booksFeature;

export const selectBooks = createSelector(
    selectBooksState,
    (state: BooksFeatureState) => {
        return state?.books;
    }
);
