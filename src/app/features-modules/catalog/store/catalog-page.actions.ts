import { createAction, createActionGroup, emptyProps, props } from '@ngrx/store';
import { Book } from 'src/app/app-interfaces';

export const booksLoadedSuccess = createAction(
    '[Books API] Books loaded',
    props<{ data: Book[] }>()
);

export const CatalogPageActions = createActionGroup({
    source: 'Books/Page',
    events: {
        'Get Books': emptyProps(),
        'Add item to cart': props<{ item: Book }>(),
        'Open Cart': emptyProps()
    }
});
