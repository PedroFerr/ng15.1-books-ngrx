import { createReducer, on } from '@ngrx/store';
import { BooksFeatureState } from 'src/app/app-interfaces';
import { booksLoadedSuccess } from './catalog-page.actions';

export const initialState: BooksFeatureState = {
  books: []
};

export const catalogReducer = createReducer(
  initialState,
  on(booksLoadedSuccess, (store, result) => {
    return {
      ...store,
      books: result.data
    }
  })
);
