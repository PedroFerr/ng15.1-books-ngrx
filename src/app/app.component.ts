import { Component, Inject, Renderer2 } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSidenav } from '@angular/material/sidenav';

import { Book } from './app-interfaces';
import { ShellState, selectNumberOfCartItems } from './shell/core.selector';

import { Observable, Subscription } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'ng15.1-books-ngrx';

    numberOfCartItems$: Observable<number> | undefined;
    private snackbarSubscription: Subscription | undefined;

    constructor(
        private titleService: Title,

        private store: Store<ShellState>,
        private matsnackbar: MatSnackBar,

        private router: Router,
        private renderer: Renderer2,
        @Inject(DOCUMENT) private document: any,
        private activatedRoute: ActivatedRoute
    ) {
        this.titleService.setTitle(this.title);
        this.updateCart();
        this.updateRouter();
    }

    private updateCart() {
        this.numberOfCartItems$ = this.store.select(selectNumberOfCartItems);
    }

    private updateRouter() {
        // const events = this.router.events
        //     .pipe(filter(event => event instanceof NavigationEnd))
        //     // //
        //     .pipe(map(() => this.activatedRoute))
        //     .pipe(map((route) => {
        //         while (route.firstChild) {
        //             route = route.firstChild;
        //         }
        //         return route;
        //     }))
        //     // //
        //     .pipe(mergeMap((route) => route.data))
        // ;
        // events.subscribe(data => this.updateBodyClass(data['bodyClass']));
    }

    private updateBodyClass(customBodyClass?: string) {
        this.renderer.setAttribute(this.document?.body, 'class', '');
        if (customBodyClass) {
            this.renderer.addClass(this.document?.body, customBodyClass);
        }
    }

    onItemAdded(book: Book, matsideNav: MatSidenav) {
        this.snackbarSubscription = this.matsnackbar
            .open(`${book.title} added to your cart.`, matsideNav.opened ? '' : 'Open Cart')
            .onAction()
            .subscribe(() => matsideNav.open())
        ;
    }

    ngOnDestroy(): void {
        this.snackbarSubscription?.unsubscribe();
    }

}
