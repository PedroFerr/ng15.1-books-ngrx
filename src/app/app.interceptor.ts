import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse
} from '@angular/common/http';
import { catchError, map, Observable, retry, throwError } from 'rxjs';

@Injectable()
export class AppInterceptor implements HttpInterceptor {

    constructor() {}

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

        const time = performance.now();

        return next.handle(request).pipe(
            retry({
                count: 2,
            }),
            map( (event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.warn(event);
                    this.logRequestTime(time, request.url, request.method);
                }
                return event
            }),

            catchError((err: any, caught: Observable<any>): any => {
                console.log(`Error handled by HTTP interceptor...`);
                return throwError(() => err)
            })
        );
    }

    private logRequestTime(initTimming: number, url: string, method: string) {
        const requestDuration: number = performance.now() - initTimming;
        console.info(`HTTP ${method} ${url} - ${requestDuration} ms`);
    }

}
