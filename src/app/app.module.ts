import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { shellReducer } from './shell/core.reducer';

import { environment } from 'src/environments/environment';

import { CustomErrorHandler } from './custom-error-handler.service';
import { AppInterceptor } from './app.interceptor';
import { AppRoutingModule } from './app-routing.module';

import { AppMaterialModule } from './app-material.module';
import { CatalogModule } from './features-modules/catalog/catalog.module';
import { CartModule } from './features-modules/shopping-cart/cart.module';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppMaterialModule,
        AppRoutingModule,

        StoreModule.forRoot(shellReducer),
        EffectsModule.forRoot([]),
        StoreDevtoolsModule.instrument({
            maxAge: 25, // Retains last 25 states
            logOnly: environment.production, // Restrict extension to log-only mode
        }),

        CatalogModule,
        CartModule,

    ],
    providers: [
        { provide: ErrorHandler, useClass: CustomErrorHandler },
        { provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
