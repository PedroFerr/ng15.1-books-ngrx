import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Book } from '../app-interfaces';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class GoogleBooksService {

    constructor(private http: HttpClient) { }

    getBooks(): Observable<Book[]> {
        return this.http
            .get<{ items: Book[] }>(environment.urlGoogleBooks)
            .pipe(
                map((books) => books.items || [])
            );
    }
}