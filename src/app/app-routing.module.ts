import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogPageComponent } from './features-modules/catalog/catalog-page/catalog-page.component';


const routes: Routes = [
    { path: '', redirectTo: '/catalogue-page', pathMatch: 'full' },
    { path: 'catalogue-page', component: CatalogPageComponent, title: 'Welcome!' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
